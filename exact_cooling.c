#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "hdf5.h"
#include "../allvars.h"
#include "../proto.h"
#include "./cooling.h"
#include <gsl/gsl_interp.h>

#ifdef COOLING

#define ZSolar   0.0129
#define TINYNUM  1.0e-32

#define NUMTEMP  161
#define NUMDENS  29
#define NUMRED   23

#define TEMP_START     1.0
#define TEMP_END       9.0
#define TEMP_GRID_SIZE 0.05

#define DENS_START   -10.0
#define DENS_END       4.0
#define DENS_GRID_SIZE 0.5


void gas_cooling(void)
{
  int i;
  for(i = FirstActiveParticle; i >= 0; i = NextActiveParticle[i])
    if(P[i].Type == 0 && P[i].Mass > 0)
      do_exact_cooling(i);	
}

double CoolingRateFromU(double u, double rho, double *ne_guess, int target)
{
  return CoolingRateFromU_table(u, rho, ne_guess, target);
}

void read_grackle_cooling_tables(void)
{
  char fname[200];
  sprintf(fname, "./CloudyData_UVB=FG2011.h5");

  if(ThisTask == 0)
    printf("BEGINNING TO READ COOLINGTABLE%s\n", fname);

  int i, j, k;
  hid_t tempfile_id, dataset;
  herr_t status;

  metal_cgrid = (double ***) malloc(NUMTEMP * sizeof(double **));
  metal_hgrid = (double ***) malloc(NUMTEMP * sizeof(double **));
  hhe_cgrid   = (double ***) malloc(NUMTEMP * sizeof(double **));
  hhe_hgrid   = (double ***) malloc(NUMTEMP * sizeof(double **));
  mmw_grid    = (double ***) malloc(NUMTEMP * sizeof(double **));

  for(j = 0; j < NUMTEMP; j++)
    {
      metal_cgrid[j] = (double **) malloc(NUMRED * sizeof(double *));
      metal_hgrid[j] = (double **) malloc(NUMRED * sizeof(double *));
      hhe_cgrid[j]   = (double **) malloc(NUMRED * sizeof(double *));
      hhe_hgrid[j]   = (double **) malloc(NUMRED * sizeof(double *));
      mmw_grid[j]    = (double **) malloc(NUMRED * sizeof(double *));

      for(k = 0; k < NUMRED; k++)
    {
      metal_cgrid[j][k] = (double *) malloc(NUMDENS * sizeof(double));
      metal_hgrid[j][k] = (double *) malloc(NUMDENS * sizeof(double));
      hhe_cgrid[j][k]   = (double *) malloc(NUMDENS * sizeof(double));
      hhe_hgrid[j][k]   = (double *) malloc(NUMDENS * sizeof(double));
      mmw_grid[j][k]    = (double *) malloc(NUMDENS * sizeof(double));

    }
    }

  double hhe_cgrid_local[NUMDENS][NUMRED][NUMTEMP];
  double hhe_hgrid_local[NUMDENS][NUMRED][NUMTEMP];
  double metal_cgrid_local[NUMDENS][NUMRED][NUMTEMP];
  double metal_hgrid_local[NUMDENS][NUMRED][NUMTEMP];
  double mmw_grid_local[NUMDENS][NUMRED][NUMTEMP];

  tempfile_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

  dataset = H5Dopen(tempfile_id, "/CoolingRates/Metals/Heating/");
  status = H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, metal_hgrid_local);
  status = H5Dclose(dataset);

  dataset = H5Dopen(tempfile_id, "/CoolingRates/Metals/Cooling/");
  status = H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, metal_cgrid_local);
  status = H5Dclose(dataset);

  dataset = H5Dopen(tempfile_id, "/CoolingRates/Primordial/Heating/");
  status = H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, hhe_hgrid_local);
  status = H5Dclose(dataset);

  dataset = H5Dopen(tempfile_id, "/CoolingRates/Primordial/Cooling/");
  status = H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, hhe_cgrid_local);
  status = H5Dclose(dataset);

  dataset = H5Dopen(tempfile_id, "/CoolingRates/Primordial/MMW/");
  status = H5Dread(dataset, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, mmw_grid_local);
  status = H5Dclose(dataset);

  for(i = 0; i < NUMTEMP; i++)
  	for(j = 0; j < NUMRED; j++)
      for(k = 0; k < NUMDENS; k++)
      	{
          metal_cgrid[i][j][k] = metal_cgrid_local[k][j][i];
          metal_hgrid[i][j][k] = metal_hgrid_local[k][j][i];
          hhe_cgrid[i][j][k] = hhe_cgrid_local[k][j][i];
          hhe_hgrid[i][j][k] = hhe_hgrid_local[k][j][i];
          mmw_grid[i][j][k] = mmw_grid_local[k][j][i];
        }

  H5Fclose(tempfile_id);

  if(ThisTask == 0)
    printf("READING COOLING TABLE DONE\n");
}

/* get cooling rate based on internal energy of particle i, also update the mean molecular weight */
double CoolingRateFromU_table(double internalenergy, double rho, double *mmw_guess, int target)
{
  double density      = rho * XH  / PROTONMASS;
  double d_n_h, d_z;
  double Z;
  int n_h_i, z_i, j, k, T_i;
  double metal_coolingrate, metal_heatingrate, hhe_coolingrate, hhe_heatingrate;
  double mugrid[NUMTEMP];
  double ugrid[NUMTEMP];
  double curves[NUMTEMP];
  double tempgrid[NUMTEMP];  
  double n_hgrid[NUMDENS];

  d_z = All.d_z;
  z_i = All.n_z_i;

  if(i > 0)
    Z = SphP[i].SmoothedZ;
  else
    Z = 0.0;

  for(T_i=0; T_i<NUMTEMP; T_i++)    
    tempgrid[T_i] = TEMP_START + T_i * TEMP_GRID_SIZE;

  for(T_i=0;T_i<NUMDENS; T_i++)     
    n_hgrid[T_i]  = DENS_START + T_i * DENS_GRID_SIZE;

  get_cooling_table_rho_index(n_hgrid, &n_h_i, &d_n_h, density);

  for(T_i=0; T_i<NUMTEMP; T_i++)
    {
      mugrid[T_i] = d_z * (d_n_h * mmw_grid[T_i][z_i][n_h_i]
                           + (1- d_n_h) * mmw_grid[T_i][z_i][n_h_i+1])
        + (1-d_z) * (d_n_h * mmw_grid[T_i][z_i+1][n_h_i]
             + (1-d_n_h) * mmw_grid[T_i][z_i+1][n_h_i+1]);

      ugrid[T_i] = BOLTZMANN * pow(10.0, tempgrid[T_i]) 
    / (mugrid[T_i] * PROTONMASS * GAMMA_MINUS1);

      metal_coolingrate = d_z * (d_n_h * metal_cgrid[T_i][z_i][n_h_i]                                                        
                 + (1-d_n_h) * metal_cgrid[T_i][z_i][n_h_i+1])                                         
    + (1-d_z) * (d_n_h * metal_cgrid[T_i][z_i+1][n_h_i]                                                            
             + (1-d_n_h) * metal_cgrid[T_i][z_i+1][n_h_i+1]);                                              
                                                                                                                   
      metal_heatingrate = d_z * (d_n_h * metal_hgrid[T_i][z_i][n_h_i]                                                        
                 + (1-d_n_h) * metal_hgrid[T_i][z_i][n_h_i +1])                                       
    + (1-d_z) * (d_n_h * metal_hgrid[T_i][z_i+1][n_h_i]                                                            
             + (1-d_n_h) * metal_hgrid[T_i][z_i+1][n_h_i+1]);          

      hhe_coolingrate = d_z * (d_n_h * hhe_cgrid[T_i][z_i][n_h_i]
                               + (1-d_n_h) * hhe_cgrid[T_i][z_i][n_h_i+1])
        + (1-d_z) * (d_n_h * hhe_cgrid[T_i][z_i+1][n_h_i]
             + (1-d_n_h) * hhe_cgrid[T_i][z_i+1][n_h_i+1]);

      hhe_heatingrate = d_z * (d_n_h * hhe_hgrid[T_i][z_i][n_h_i]
                               + (1-d_n_h) * hhe_hgrid[T_i][z_i][n_h_i+1])
        + (1-d_z) * (d_n_h * hhe_hgrid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * hhe_hgrid[T_i][z_i+1][n_h_i+1]);

      curves[T_i] = hhe_heatingrate  - hhe_coolingrate + 
                    Z / ZSolar * metal_heatingrate - Z / ZSolar * metal_coolingrate;
    }
  
  if(internalenergy < ugrid[0])    
    internalenergy = ugrid[0];
    
  if(internalenergy > ugrid[NUMTEMP-1])    
    internalenergy = ugrid[NUMTEMP-1];

  *mmw_guess    = interpol(internalenergy, NUMTEMP, ugrid, mugrid);

return interpol(internalenergy, NUMTEMP, ugrid, curves);
}

/* Generate the cooling curve for particle i with all possible temperature present in the cooling table and then 
carry out the integration of internal energy due to gas cooling. */
void do_exact_cooling(int i)
{
  double dt;
#ifndef WAKEUP
  dt = (P[i].TimeBin ? (1 << P[i].TimeBin) : 0) * All.Timebase_interval;
#else
  dt = P[i].dt_step * All.Timebase_interval;
#endif
  double internalenergy = DMAX(All.MinEgySpec, SphP[i].InternalEnergy);
  double rho = All.cf_a3inv * SphP[i].Density * All.UnitDensity_in_cgs 
    * All.HubbleParam;
  /* convert to physical cgs units */

  internalenergy *= All.UnitPressure_in_cgs / All.UnitDensity_in_cgs;
  dt *= All.UnitTime_in_s / All.HubbleParam / All.cf_hubble_a;
  /*actual time step in second */

  double density = XH * rho / PROTONMASS;
  double ratefact = density * density / rho;
  double d_n_h, d_z;
  int n_h_i, z_i, j, k, T_i;

  z_i = All.n_z_i;
  d_z = All.d_z;

  double Z = 0.0;
    
  double metal_coolingrate, metal_heatingrate;
  double hhe_coolingrate, hhe_heatingrate;

  double finalu, du;

  double      mugrid[NUMTEMP];
  double  ugrid_temp[NUMTEMP];
  double      curves[NUMTEMP];
  double heatingrate[NUMTEMP];
  double coolingrate[NUMTEMP];

  double tempgrid[NUMTEMP];
  double n_hgrid[NUMDENS];

  double zero_point = 1e30;
  int    int_zero_point = -1;
    
  int    int_next_zero_point;
  double u1, u2, c1, c2, h1, h2, internalenergy_zero;;
    
  for(T_i=0; T_i<NUMTEMP; T_i++)
    tempgrid[T_i] = TEMP_START + T_i * TEMP_GRID_SIZE;

  for(T_i=0;T_i<NUMDENS; T_i++)
    n_hgrid[T_i]  = DENS_START + T_i * DENS_GRID_SIZE;

  if(i > 0)
    Z = SphP[i].SmoothedZ;
  else
    Z = 0.0;

  /* generate cooling curves given density and redshift for this particle */
  get_cooling_table_rho_index(n_hgrid, &n_h_i, &d_n_h, density);

  for(T_i = 0; T_i < NUMTEMP; T_i++)
    {
      mugrid[T_i] = d_z * (d_n_h * mmw_grid[T_i][z_i][n_h_i]
                           + (1-d_n_h) * mmw_grid[T_i][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * mmw_grid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * mmw_grid[T_i][z_i + 1][n_h_i+1]);

      /* mugrid[T_i] = 1.0 / (1.0/mugrid[T_i] + Z / 16.0);  correction term for mu from metals */
        
      ugrid_temp[T_i] = (BOLTZMANN * pow(10.0, tempgrid[T_i]) 
             / (mugrid[T_i] * PROTONMASS * GAMMA_MINUS1));

      metal_coolingrate = d_z * (d_n_h * metal_cgrid[T_i][z_i][n_h_i]
                                 + (1-d_n_h) * metal_cgrid[T_i][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * metal_cgrid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * metal_cgrid[T_i][z_i + 1][n_h_i+1]);

      metal_heatingrate = d_z * (d_n_h * metal_hgrid[T_i][z_i][n_h_i]
                                 + (1-d_n_h) * metal_hgrid[T_i][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * metal_hgrid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * metal_hgrid[T_i][z_i + 1][n_h_i+1]);

      hhe_coolingrate = d_z * (d_n_h * hhe_cgrid[T_i][z_i][n_h_i]
                               + (1-d_n_h) * hhe_cgrid[T_i][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * hhe_cgrid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * hhe_cgrid[T_i][z_i+1][n_h_i+1]);

      hhe_heatingrate = d_z * (d_n_h * hhe_hgrid[T_i][z_i][n_h_i]
                               + (1-d_n_h) * hhe_hgrid[T_i][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * hhe_hgrid[T_i][z_i + 1][n_h_i]
             + (1-d_n_h) * hhe_hgrid[T_i][z_i+1][n_h_i+1]);
        
      heatingrate[T_i] = hhe_heatingrate + Z / ZSolar * metal_heatingrate;
        
      coolingrate[T_i] = hhe_coolingrate + Z / ZSolar * metal_coolingrate;

      curves[T_i]      = heatingrate[T_i] - coolingrate[T_i];
        
      if(fabs(curves[T_i]) < zero_point)
        {
            zero_point = fabs(curves[T_i]);
            int_zero_point = T_i;
        }
    }
    
  int_next_zero_point = int_zero_point;

    /*find zero point where heating rate equals cooling rate*/
   if(curves[0]>TINYNUM)
    {
        if((int_zero_point>1) && (int_zero_point<NUMTEMP-2))
        {
            if((curves[int_zero_point] * curves[int_zero_point-1]) < 0)
                int_next_zero_point = int_zero_point-1;
            else if((curves[int_zero_point] * curves[int_zero_point+1]) < 0)
                int_next_zero_point = int_zero_point+1;
            
            if(int_next_zero_point != int_zero_point)
            {
                u1 = ugrid_temp[int_zero_point];
                u2 = ugrid_temp[int_next_zero_point];
                c1 = coolingrate[int_zero_point];
                c2 = coolingrate[int_next_zero_point];
                h1 = heatingrate[int_zero_point];
                h2 = heatingrate[int_next_zero_point];
                internalenergy_zero = u1 + (h1-c1)*(u2-u1)/(c2-c1+h1-h2);
                
                ugrid_temp[int_zero_point] = internalenergy_zero;
                curves[int_zero_point] = 0.0;
            }
        }
    }
    

  finalu = exact_cooling_integration(internalenergy, dt, density, ugrid_temp, curves);

  finalu *=  All.UnitDensity_in_cgs / All.UnitPressure_in_cgs;
    
  du = finalu - SphP[i].InternalEnergy;

  if(du < -0.5 * SphP[i].InternalEnergy)
    du  = -0.5 * SphP[i].InternalEnergy;

  SphP[i].InternalEnergy += du;
  SphP[i].MMW = interpol(finalu, NUMTEMP, ugrid_temp, mugrid); 
  SphP[i].InternalEnergyPred = SphP[i].InternalEnergy;  
  SphP[i].Pressure = get_pressure(i);

}


/*
 Input internal energy, time step dt, internal energy grid, cooling curve should all in cgs units and
 NUMTEMP is the number of data points in the cooling curve/energy grids.
 */

double exact_cooling_integration(double internalenergy, double dt, double density, double* ugrid, double* curves)
{
    double ratefact = density * XH / PROTONMASS;
    double A, B, deltaY, factpre;
    double Ys[NUMTEMP]; /* ugrid <----> Ys */
    int T_i;
    double thisY, coolingtime, uref, coolref;
    double thisrate, finalu, dY;
    
    if(internalenergy < ugrid[0])
        internalenergy = ugrid[0];
    
    if(internalenergy > ugrid[NUMTEMP-1])
        internalenergy = ugrid[NUMTEMP-1];
    
    thisrate = interpol(internalenergy, NUMTEMP, ugrid, curves);
    
    /*   cooling time in seconds  */
    coolingtime = internalenergy / (ratefact * thisrate);
    
    if((thisrate >  TINYNUM) || (thisrate < -TINYNUM))
    {
        if(thisrate < 0) 	/* we have cooling */
        {
            for(T_i=0; T_i < NUMTEMP; T_i++)
            {
                Ys[T_i] = 0.0;
                if(curves[T_i] > -TINYNUM)
                  curves[T_i] = -TINYNUM;
            }
            
            uref    = ugrid[NUMTEMP-1];
            coolref = curves[NUMTEMP-1];
            factpre = coolref/uref;
            
            for(T_i=NUMTEMP-2; T_i>=0; T_i--)
            {                
                Ys[T_i] = Ys[T_i+1];
                
                if(curves[T_i+1] != curves[T_i])
                {
                    A = (ugrid[T_i+1]-ugrid[T_i])/(curves[T_i+1]-curves[T_i]);
                    B = (ugrid[T_i+1]*curves[T_i]-ugrid[T_i]*curves[T_i+1])
                    /(curves[T_i+1]- curves[T_i]);
                    deltaY = factpre*A*log((B + ugrid[T_i+1])/(B + ugrid[T_i]));
                }
                else
                    deltaY = factpre* ((ugrid[T_i+1]-ugrid[T_i])/curves[T_i]);
                
                Ys[T_i]   -= deltaY;
            }
        }
        else  /* we have heating */
        {
            for(T_i=0; T_i < NUMTEMP; T_i++)
            {
                Ys[T_i] = 0.0;
                
                if(curves[T_i] < TINYNUM)
                    curves[T_i] = TINYNUM;
            }
            
            uref    = ugrid[0];
            coolref = curves[0];
            factpre = coolref/uref;
            
            for(T_i=1; T_i<NUMTEMP; T_i++)
            {
                
                Ys[T_i]  = Ys[T_i-1];
                
                if(curves[T_i] != curves[T_i-1])
                {
                    A = (ugrid[T_i] - ugrid[T_i-1])/(curves[T_i]-curves[T_i-1]);
                    B = (ugrid[T_i]*curves[T_i-1]-ugrid[T_i-1]*curves[T_i])
                    /(curves[T_i] - curves[T_i-1]);
                    deltaY = factpre*A*log((B + ugrid[T_i])/(B + ugrid[T_i-1]));
                }
                else
                    deltaY = factpre*((ugrid[T_i]-ugrid[T_i-1])/curves[T_i-1]);
                
                Ys[T_i]   +=  deltaY;
            }
        }
        
        /*changes of Y measured at this internal energy*/
        dY=(coolref/uref)*fabs(internalenergy/thisrate)*fabs(dt/coolingtime);
        
        if(thisrate  <  0) /* we have cooling */
        {
            for(T_i = NUMTEMP-2; T_i>0; T_i--)
                if (ugrid[T_i] < internalenergy)
                    break;
            
            if(curves[T_i+1] != curves[T_i])
            {
                A = (ugrid[T_i+1]-ugrid[T_i])/(curves[T_i+1]-curves[T_i]);

                B = (ugrid[T_i+1]*curves[T_i]-ugrid[T_i]*curves[T_i+1])
                    /(curves[T_i+1]-curves[T_i]);

                thisY = Ys[T_i+1]-factpre*A*log((B+ugrid[T_i+1])/(B+internalenergy));
            }
            else
                thisY = Ys[T_i+1]-factpre*((ugrid[T_i+1]-internalenergy)/curves[T_i+1]);
            
            thisY += dY;
            
            if(thisY < Ys[0])
                thisY = Ys[0];
            
            for(; T_i>=0; T_i--)
                if(Ys[T_i] < thisY)
                    break;
            
            deltaY = thisY - Ys[T_i+1];
            
            if(curves[T_i+1] != curves[T_i])
            {
                A = (ugrid[T_i+1]-ugrid[T_i])/(curves[T_i+1]-curves[T_i]);
            
                B = (ugrid[T_i+1]*curves[T_i]-ugrid[T_i]*curves[T_i+1])
                    /(curves[T_i+1]-curves[T_i]);
                
                finalu = (B+ugrid[T_i+1])*exp(deltaY/(A*factpre))-B;
            }
            else
                finalu = ugrid[T_i+1]+deltaY*curves[T_i+1]/factpre;
        }
        else
        {	  /*we have heating*/
            for(T_i=1; T_i<NUMTEMP; T_i++)
                if (ugrid[T_i] > internalenergy)
                    break;
            
            if(curves[T_i] != curves[T_i-1])
            {
                A = (ugrid[T_i]-ugrid[T_i-1])/(curves[T_i]-curves[T_i-1]);
                B = (ugrid[T_i]*curves[T_i-1]-ugrid[T_i-1]*curves[T_i])
                    /(curves[T_i]-curves[T_i-1]);
                thisY = Ys[T_i-1]+factpre*A*log((B+internalenergy)/(B + ugrid[T_i-1]));
            }
            else            
                thisY = Ys[T_i-1]+factpre*((internalenergy-ugrid[T_i-1])/curves[T_i-1]);            
            
            thisY += dY;
            
            for(; T_i<NUMTEMP; T_i++)
                if(Ys[T_i] > thisY)
                    break;
            
            deltaY = thisY - Ys[T_i-1];
            
            if(curves[T_i] != curves[T_i-1])
            {
                A = (ugrid[T_i]-ugrid[T_i-1])/(curves[T_i]-curves[T_i-1]);
                
                B = (ugrid[T_i]*curves[T_i-1]-ugrid[T_i-1]*curves[T_i])
                    /(curves[T_i]-curves[T_i-1]);
            
                finalu = (B+ugrid[T_i-1])*exp(deltaY/(factpre*A))-B;
            }
            else
                finalu = ugrid[T_i-1]+deltaY * curves[T_i-1]/factpre;
        }
    }
    else
        finalu = internalenergy + ratefact*thisrate*dt;
    
    return finalu;
}

/* Get cooling rate from temperature and density for target particle */
double GetCoolingRate(int target, double temperature, double density)
{
  double t_coolingrate;
  double d_n_h, d_T, d_z;
  int n_h_i, T_i, z_i;
  double Z = 0.0;
  int k;

  z_i = All.n_z_i;
  d_z = All.d_z;

  if(target >= 0)
    Z = SphP[target].SmoothedZ;
  else
    Z = 0.0;

  double tempgrid[NUMTEMP];
  double n_hgrid[NUMDENS];

  for(T_i=0; T_i<NUMTEMP; T_i++)    
    tempgrid[T_i] = TEMP_START + T_i * TEMP_GRID_SIZE;

  for(T_i=0;T_i<NUMDENS; T_i++)     
    n_hgrid[T_i]  = DENS_START + T_i * DENS_GRID_SIZE; 

  get_cooling_table_index(n_hgrid, tempgrid, &n_h_i, &d_n_h, &T_i, &d_T, temperature, density);

  double metal_coolingrate = d_z * (d_n_h * d_T * metal_cgrid[T_i][z_i][n_h_i]
                    + (1-d_n_h) * d_T * metal_cgrid[T_i][z_i][n_h_i+1]
                    + d_n_h * (1-d_T) * metal_cgrid[T_i + 1][z_i][n_h_i]
                    + (1-d_n_h) * (1-d_T) * metal_cgrid[T_i + 1][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * d_T * metal_cgrid[T_i][z_i+1][n_h_i] 
         + (1-d_n_h) * d_T * metal_cgrid[T_i][z_i+1][n_h_i+1] 
         + d_n_h * (1-d_T) * metal_cgrid[T_i+1][z_i+1][n_h_i] 
         + (1-d_n_h) * (1-d_T) * metal_cgrid[T_i+1][z_i+1][n_h_i+1]);

  double metal_heatingrate = d_z * (d_n_h * d_T * metal_hgrid[T_i][z_i][n_h_i]
                    + (1-d_n_h) * d_T * metal_hgrid[T_i][z_i][n_h_i+1]
                    + d_n_h * (1-d_T) * metal_hgrid[T_i+1][z_i][n_h_i]
                    + (1-d_n_h) * (1-d_T) * metal_hgrid[T_i+1][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * d_T * metal_hgrid[T_i][z_i+1][n_h_i] 
             + (1-d_n_h) * d_T * metal_hgrid[T_i][z_i+1][n_h_i+1] 
             + d_n_h * (1-d_T) * metal_hgrid[T_i+1][z_i+1][n_h_i] 
             + (1-d_n_h) * (1-d_T) * metal_hgrid[T_i+1][z_i+1][n_h_i+1]);

  double hhe_heatingrate = d_z * (d_n_h * d_T * hhe_hgrid[T_i][z_i][n_h_i]
                  + (1-d_n_h) * d_T * hhe_hgrid[T_i][z_i][n_h_i+1]
                  + d_n_h * (1-d_T) * hhe_hgrid[T_i+1][z_i][n_h_i]
                  + (1-d_n_h) * (1-d_T) * hhe_hgrid[T_i+1][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * d_T * hhe_hgrid[T_i][z_i+1][n_h_i] 
             + (1-d_n_h) * d_T * hhe_hgrid[T_i][z_i+1][n_h_i+1] 
             + d_n_h * (1-d_T) * hhe_hgrid[T_i+1][z_i + 1][n_h_i] 
             + (1-d_n_h) * (1-d_T) * hhe_hgrid[T_i+1][z_i+1][n_h_i+1]);

  double hhe_coolingrate = d_z * (d_n_h * d_T * hhe_cgrid[T_i][z_i][n_h_i]
                  + (1-d_n_h) * d_T * hhe_cgrid[T_i][z_i][n_h_i+1]
                  + d_n_h * (1-d_T) * hhe_cgrid[T_i+1][z_i][n_h_i]
                  + (1-d_n_h) * (1-d_T) * hhe_cgrid[T_i+1][z_i][n_h_i+1])
    + (1-d_z) * (d_n_h * d_T * hhe_cgrid[T_i][z_i+1][n_h_i] 
             + (1-d_n_h) * d_T * hhe_cgrid[T_i][z_i+1][n_h_i+1] 
             + d_n_h * (1-d_T) * hhe_cgrid[T_i+1][z_i+1][n_h_i] 
             + (1-d_n_h) * (1-d_T) * hhe_cgrid[T_i+1][z_i+1][n_h_i+1]);

  return (hhe_heatingrate - hhe_coolingrate) + 
    (Z / ZSolar) * (metal_heatingrate - metal_coolingrate);	
}


void get_cooling_table_index(double *n_hgrid, double *tempgrid, int *n_h_i, double *d_n_h, 
 int *T_i, double *d_T, double inn_T, double inn_h)
{
  if(log10(inn_h) <= DENS_START)
    {
      *d_n_h = 1.0;       *n_h_i = 0;
    }
  else if(log10(inn_h) >= DENS_END)
    {
      *d_n_h = 0.0;       *n_h_i = NUMDENS - 2;
    }
  else
    {
      *n_h_i = (int) floor((log10(inn_h) -  DENS_START) / DENS_GRID_SIZE);     
      *d_n_h = ((*n_h_i+1.0)* DENS_GRID_SIZE + DENS_START - log10(inn_h)) / DENS_GRID_SIZE;
    }

  if(inn_T <= TEMP_START)
    {
      *d_T = 1.0;            *T_i = 0;
    }
  else if(inn_T >= TEMP_END)
    {
      *d_T = 0.0;           *T_i = NUMTEMP - 2;
    }
  else
    {
      *T_i = (int) floor((inn_T - tempgrid[0]) / (tempgrid[1] - tempgrid[0]));
      *d_T = (tempgrid[(*T_i) + 1] - inn_T) / (tempgrid[(*T_i) + 1] - tempgrid[*T_i]);
    }
}


void get_cooling_table_rho_index(double *n_hgrid, int *n_h_i, double *d_n_h, double inn_h)
{  
  if(log10(inn_h) <= DENS_START)    
    {
      *d_n_h = 1.0;       *n_h_i = 0;    
    }
  else if(log10(inn_h) >= DENS_END)    
    {
      *d_n_h = 0.0;       *n_h_i = NUMDENS - 2;
    }
  else    
    {
      *n_h_i = (int) floor((log10(inn_h) -  DENS_START) / DENS_GRID_SIZE);          
      *d_n_h = ( (*n_h_i+1.0)* DENS_GRID_SIZE + DENS_START - log10(inn_h)) / DENS_GRID_SIZE;    
    }
}

void get_cooling_table_z_index(int *n_z_i, double *d_z, double inn_z)
{
  double zgrid[23] = { 0.0, 0.12202, 0.25893, 0.41254, 0.58489, 
               0.77828, 0.99526, 1.2387, 1.5119, 1.8184, 
               2.1623,  2.5481, 2.9811, 3.4668, 4.0119,  
               4.6234, 5.3096, 6.0795, 6.9433, 7.9125, 
               9.0000, 10.22, 10.22};
  int k;

  if(All.ComovingIntegrationOn)
    {
      if(inn_z <= 0.12202)      
    {
      *d_z = 1.0;     *n_z_i = 0;   
    }
      else if(inn_z >= 10.22)   
    {
      *d_z = 0.0;     *n_z_i = NUMRED - 2;  
    }
      else      
    {
      for(k = 0; k < NUMRED; k++)       
        {
          if(zgrid[k] <= inn_z && zgrid[k + 1] > inn_z)
        break;      
        }
      *n_z_i = k;
      *d_z = (zgrid[(*n_z_i)+1]-inn_z) / (zgrid[(*n_z_i)+1]-zgrid[*n_z_i]);       
    }
    }
  else   
    {
      *d_z = 1.0;      *n_z_i = 0;    
    }
}


double interpolx(double x, int n, double *xg, int *i0, int *i1) 
{
  double f;
  int i;

  /*increasing order array*/
  if(xg[0] < xg[n-1])
    {
      if(x <= xg[0])       
    *i0 = 0;
      else if(x >= xg[n-1])     
    *i0 = n-2;
      else
    {
      *i0 = 0;
      *i1 = n-1;
      while(*i1 - *i0 > 1)
        {
          i = (*i0 + *i1)/2;
          if (x <= xg[i])             
        *i1 = i;
          else                
        *i0 = i;
        }
    }
    }
  else     /*decreasing order arrarys*/
    {   
      if(x >= xg[0])   
    *i0 = 0;
      else if(x <= xg[n-1])     
    *i0 = n-2;
      else      
    {
      *i0 = 0;
      *i1 = n-1;
      while(*i1 - *i0 > 1)
        {
          i = ( *i0 + *i1) / 2;
          if (x >= xg[i])             
        *i1 = i;
          else                
        *i0 = i;
        }
    }
    }

  f = (x - xg[*i0])/(xg[*i0+1] - xg[*i0]);
  *i1 = *i0 + 1;
  
  if(f > 1.0)   	f = 1.0;
  if(f < 0.0)   	f = 0.0;

  return f;
}

double interpol(double x, int n, double *xg, double *yg) 
{
  double f;
  int i0, i1;
  f = interpolx(x, n, xg, &i0, &i1);
  return (1.0-f)*yg[i0] + f*yg[i1];
}
