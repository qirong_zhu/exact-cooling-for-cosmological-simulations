This is a piece of C code to perform gas cooling using the exact integration method proposed by 
R.H.D. Townsend 2009. It includes radiative cooling, redshift-dependent UVB heating, and metal-dependence. 

A). Download the coolingtable of 'CloudyData_UVB=FG2011.h5' (Faucher-Giguere et. al. 2009 with 2011 update) 
from Grackle website: 
https://grackle.readthedocs.io/en/grackle-3.0/. 

Also can use the "CloudyData_UVB=HM2012.h5" (Haardt & Madau 2012) instead. The redshift array in exact_cooling.c
then needs to be matched to what is present in "CloudyData_UVB=HM2012.h5". 

B). Download the public version of Gizmo from 
http://www.tapir.caltech.edu/~phopkins/Site/GIZMO.html 
or Gadget-2 code from 
http://gadgetcode.org/.

In 'All' structure of allvars.h, add double d_z and int n_z_i. 

If metal-dependent cooling is required, add 'MyFloat SmoothedZ' into 'SPH' structure to include an estimate of 
local metallicity density. 

C). Complete the following modifications to the source code. The example below is based on Gizmo. It should be 
very similar for Gadget. 

(c1). In 'begrun.c', call read_grackle_cooling_tables() and include the header 'exact_cooling.h' there. 

(c2). In each calculate_non_standard_physics() of 'run.c', call 
get_cooling_table_z_index(&All.n_z_i, &All.d_z, 1.0/All.Time - 1) to update the pointer to the redshift array 
in the cooling table. Also include the header file 'exact_cooling.h' here. 

(c3). Simply call gas_cooling() within calculate_non_standard_physics() in 'run.c' to integrate gas internal 
energy in each step. 

D). If you use this code, please kindly cite the references: Zhu, Smith & Hernquist 2017, 
Grackle paper Smith et. al 2017 (http://adsabs.harvard.edu/abs/2017MNRAS.466.2217S) 
and also R. H. D. Townsend 2009 paper (http://adsabs.harvard.edu/abs/2009ApJS..181..391T). 