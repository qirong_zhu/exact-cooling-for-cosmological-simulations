double ***metal_cgrid;
double ***metal_hgrid;
double ***hhe_cgrid;
double ***hhe_hgrid;
double ***mmw_grid;

void read_grackle_cooling_tables(void);

double GetCoolingRate(int target, double temperature, double density);


void get_cooling_table_index(double *n_hgrid, double *tempgrid,
                             int *n_h_i, double *d_n_h, int *T_i, double *d_T,
                             double inn_T, double inn_h);


void get_cooling_table_rho_index(double *n_hgrid, int *n_h_i, double *d_n_h, double inn_h);

void get_cooling_table_z_index(int *n_z_i, double *d_z, double inn_z);

double CoolingRateFromU_table(double internalenergy, double rho, double *ne_guess, int target);

void do_exact_cooling(int i);
double  exact_cooling_integration
(double internalenegy, double dt, double density, double* ugrid, double* curves);

double interpol(double x, int n, double *xg, double *yg);
double interpolx(double x, int n, double *xg, int *i0, int *i1);
